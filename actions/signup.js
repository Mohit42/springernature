const express = require('express');
const router = express.Router();

router.get('/', (req,res) => {
	res.render('../views/signupForm.pug', { title: 'Sign-up form' });
});

router.post('/', (req, res) => {
	const body = req.body; 
	const pugObj = {
		firstname: body.firstname,
		lastname: body.lastname,
		email: body.email,
		title: 'Sign-up completed',
		newsletter: false
	}
	if(body.check === 'on') {
		pugObj.newsletter = true;
	}
	res.render('../views/signupCompletion.pug', pugObj);
});

module.exports = router;