# SpringerNature


**Prerequisites:**

        node
        npm

**Project Setup:**

1.  Go to SpringerNature gitlab repository and clone the repository or download it.

2.  Open springernature folder which you just cloned/downloaded.

3.  Install required npm packages using the command mentioned below:

         $ npm Install

5.  Run the server:
         `node index.js`


**Apis:**

<details><summary>Get signup form</summary>

* **URL**

  http://localhost:8000/user/signup

* **Method:**

  `GET`

</details>


