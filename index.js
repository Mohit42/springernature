const express = require('express');
const router = express.Router();
const signupRoutes = require('./paths/signupform');

const app = express();
const port = 8000;

app.set('view engine', 'pug');
router.use(express.urlencoded({
    extended: true
}))
app.use(router);

router.use('/user',signupRoutes);

app.listen(port, () => {
	console.log('listening...');
});
