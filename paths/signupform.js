const express = require('express');
const signup = require('../actions/signup');
const router = express.Router();

router.use('/signup',signup);

module.exports = router;